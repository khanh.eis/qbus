# vue-uber-bus [![Build Status](https://travis-ci.org/vivubus/vivubus-frontend.svg?branch=master)](https://travis-ci.org/vivubus/vivubus-frontend)

Member: KhanhND, CuongVT
## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn run serve
```

### Compiles and minifies for production
```
yarn run build
```

### Run your tests
```
yarn run test
```

### Lints and fixes files
```
yarn run lint
```
