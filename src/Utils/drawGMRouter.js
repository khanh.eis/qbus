export function drawGMRouter(_startGM, _endGM, _map, _directionsDisplay, _travelMode = 'DRIVING', _displayPanelID = "") {
    return new Promise((resolve, reject) => {
        try {
            let directionsService = new window.google.maps.DirectionsService();
            if (_directionsDisplay.setMap == undefined) {
                _directionsDisplay = new window.google.maps.DirectionsRenderer();
                _directionsDisplay.setMap(_map);
                if (_displayPanelID != "") _directionsDisplay.setPanel(document.getElementById(_displayPanelID));
            }
            else {
                _directionsDisplay.setDirections({ routes: [] });
            }
            _directionsDisplay.setOptions({
                polylineOptions: {
                    strokeColor: 'green',
                    strokeOpacity: 0.5,
                    strokeWeight: 2
                },
                // markerOptions: {
                //     icon: '/bustop.png'
                // }
            });

            let request = {
                origin: _startGM,
                destination: _endGM,
                travelMode: _travelMode
            };
            directionsService.route(request, function(result, status) {
                if (status == 'OK') {
                    console.log(result)
                    _directionsDisplay.setDirections(result);
                    resolve();
                }
                reject(Error("Request faile: " + JSON.stringify(result)))
            });
        }
        catch (e) {
            reject(Error(e))
        }
    })
}
