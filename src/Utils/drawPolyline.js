export function drawPolyline(_arrayLatLng, _map, _polyline) {
    return new Promise((resolve, reject) => {
        try{
        if (_polyline.setMap == undefined) {
            _polyline = new window.google.maps.Polyline({
                geodesic: true,
                strokeColor: '#FF0000',
                strokeOpacity: 1.0,
                strokeWeight: 2
            });
            _polyline.setMap(_map);
        }
        else {
            _polyline.setMap(null);
        }
        _polyline.setPath(_arrayLatLng);
        resolve(_polyline);
        }
        catch(e){
            reject(Error("Draw polyline err: " + JSON.stringify(e)))
        }
    })
}
