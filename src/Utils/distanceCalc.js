export function distanceCalc(start, end) {
    return getDistanceFromLatLonInKm(start.lat(),start.lng(),end.lat(),end.lng())
    // export function distanceCalc(start, end, waypts = [], travelMode = 'DRIVING') {
    // return new Promise((resolve, reject) => {
    //     console.log(`-->Bat dau tinh khoang cach giua ${start.lat()}, ${start.lng()} va ${end.lat()},${end.lng()}: dau ham`)
    //     if (typeof window.google != 'object') reject(Error("Google Map API not loaded"))
    //     let directionsService = new window.google.maps.DirectionsService();
    //     let request = {
    //         origin: start,
    //         destination: end,
    //         travelMode: travelMode,
    //         waypoints: waypts
    //     };
    //     directionsService.route(request, function(result, status) {
    //         console.log(`-->Bat dau tinh khoang cach giua ${start.lat()}, ${start.lng()} va ${end.lat()},${end.lng()}: da tra ve request ${JSON.stringify(result)}, status ${JSON.stringify(status)}`)
    //         if (status == 'OK') {
    //             let distance = 0;
    //             result.routes[0].legs.forEach(leg => {
    //                 distance += leg.distance.value;
    //             })
    //             console.log(`-->Bat dau tinh khoang cach giua ${start.lat()}, ${start.lng()} va ${end.lat()},${end.lng()}: da tinh khoang cach la ${distance}`)

    //             resolve(distance);
    //         }
    //         else {
    //             resolve(0);
    //         }
    //     });
    // })
}
function getDistanceFromLatLonInKm(lat1,lon1,lat2,lon2) {
  var R = 6371; // Radius of the earth in km
  var dLat = deg2rad(lat2-lat1);  // deg2rad below
  var dLon = deg2rad(lon2-lon1); 
  var a = 
    Math.sin(dLat/2) * Math.sin(dLat/2) +
    Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) * 
    Math.sin(dLon/2) * Math.sin(dLon/2)
    ; 
  var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a)); 
  var d = R * c; // Distance in km
  return d;
}

function deg2rad(deg) {
  return deg * (Math.PI/180)
}