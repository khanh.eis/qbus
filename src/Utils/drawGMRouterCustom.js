export function drawGMRouterCustom(_startGM, _endGM, _map, _polyline, _travelMode = 'DRIVING', _displayPanelID = "") {
    return new Promise((resolve, reject) => {
        try {
            let directionsService = new window.google.maps.DirectionsService();
            if (_polyline.setMap == undefined) {
                _polyline = new window.google.maps.Polyline({
                    geodesic: true,
                    strokeColor: 'green',
                    strokeOpacity: 0.3,
                    strokeWeight: 2
                });
                _polyline.setMap(_map);
            }
            else {
                _polyline.setMap(null);
            }

            let request = {
                origin: _startGM,
                destination: _endGM,
                travelMode: _travelMode
            };
            directionsService.route(request, function(result, status) {
                if (status == 'OK') {
                    console.log(result)
                    _polyline.setPath(result.routes[0].overview_path);
                    setPanelText(_displayPanelID,result);
                    resolve(_polyline);
                }
                reject(Error("Request faile: " + JSON.stringify(result)))
            });
        }
        catch (e) {
            reject(Error(e))
        }
    })
}
function setPanelText(id,result){
    let element=document.getElementById(id);
    let textGen=""
    let leg=result.routes[0].legs[0];
    
    textGen+=`
    <p>Đi bộ từ ${leg.start_address} <small>(<b>Khoảng ${leg.distance.text}</b>, ${leg.duration.text})</small></p>
    `
    let steps=leg.steps;
    for(let i=0;i<steps.length;i++){
        let step = steps[i]
        textGen+=`
        <p><b>${i+1}.</b> ${step.instructions} (${step.distance.text})</p>
        `
    }
    
    
    element.innerHTML=textGen;
}