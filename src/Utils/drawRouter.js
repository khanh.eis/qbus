export function drawRouter(_nameRouter, _listRouters, _map, _directionsDisplay, _displayPanelID = "") {
    return new Promise((resolve, reject) => {
        try {
            let Router = _listRouters.filter(e => e.name == _nameRouter)[0];
            let directionsService = new window.google.maps.DirectionsService();
            if (_directionsDisplay.setMap == undefined) {
                _directionsDisplay = new window.google.maps.DirectionsRenderer();
                _directionsDisplay.setMap(_map);
                if (_displayPanelID != "") _directionsDisplay.setPanel(document.getElementById(_displayPanelID));
            }
            else {
                _directionsDisplay.setDirections({ routes: [] });
            }
            _directionsDisplay.setOptions({
                polylineOptions: {
                    strokeColor: 'blue',
                    strokeOpacity: 0.2,
                    strokeWeight: 5
                },
                markerOptions: {
                    icon: '/bustop.png'
                }
            });
            let start = new window.google.maps.LatLng(String(Router.wayPoints[0].x), String(Router.wayPoints[0].y));
            let end = new window.google.maps.LatLng(String(Router.wayPoints[Router.wayPoints.length - 1].x), String(Router.wayPoints[Router.wayPoints.length - 1].y));
            let waypts = [];
            for (let i = 1; i < Router.wayPoints.length - 1; i++) {
                let e = Router.wayPoints[i];
                let waypt = {
                    location: new window.google.maps.LatLng(e.x, e.y),
                    stopover: e.busStop ? true : false
                }
                waypts.push(waypt);
            }
            let request = {
                origin: start,
                destination: end,
                waypoints: waypts,
                travelMode: 'DRIVING'
            };
            directionsService.route(request, function(result, status) {
                if (status == 'OK') {
                    console.log(result)
                    _directionsDisplay.setDirections(result);
                    setTimeout(() => {
                        for (let i = 0; i < document.getElementsByClassName("adp-marker2").length; i++) {
                            document.getElementsByClassName("adp-marker2")[i].src = "/bustop.png";
                            document.getElementsByClassName("adp-marker2")[i].style = "height:auto;width:auto";
                        }
                    }, 1000)
                    resolve(_directionsDisplay)
                }
            });
        }
        catch (e) {
            reject(Error(e))
        }
    })
}
