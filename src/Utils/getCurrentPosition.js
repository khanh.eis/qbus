export function getCurrentPosition(options) {
    return new Promise((resolve, reject)=> {
        if (window.navigator.geolocation) {
                window.navigator.geolocation.getCurrentPosition(resolve, reject, options);
        }
        else {
            reject(Error('Error getCurrentPosition.'))
        }

    })
}
