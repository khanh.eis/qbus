import Vue from 'vue'
import Vuex from 'vuex'
Vue.use(Vuex)

export default new Vuex.Store({
    state: {
        map: {},
        listRouters: [{
                name: "test",
                wayPoints: [
                    { x: 17.4791905, y: 106.6182792, busStop: true },
                    { x: 17.487971, y: 106.627356, busStop: true },
                    { x: 17.471276, y: 106.622292, busStop: true }
                ]
            }, {
                name: 'Tuyến B1: TP. Đồng Hới - Thị xã Ba Đồn',
                wayPoints: [
                    { x: 17.4478584, y: 106.5777891 , busStop: true },
                    { x: 17.4638814, y: 106.6240317, busStop: true },
                    { x: 17.4711313, y: 106.622266, busStop: true },
                    { x: 17.466898, y: 106.6044638, busStop: true },
                    { x: 17.4722561, y: 106.6042974, busStop: true },
                    { x: 17.4846466, y: 106.6039467, busStop: true },
                    { x: 17.7517166, y: 106.4419572, busStop: true },
                    { x: 17.7567581, y: 106.4209925, busStop: true },
                    { x: 17.7531205, y: 106.4209711, busStop: true },
                    { x: 17.7522205, y: 106.4202398 , busStop: true },
                ]
            },
            {
                name: 'Tuyến B2: TP. Đồng Hới - Huyện Lệ Thủy',
                wayPoints: [
                    { x: 17.4848067, y: 106.5763883, busStop: true  },
                    { x: 17.4689741, y: 106.5949502, busStop: true },
                    { x: 17.4658912, y: 106.5983939, busStop: true },
                    { x: 17.4667682, y: 106.6045556, busStop: true },
                    { x: 17.472359, y: 106.6042474, busStop: true },
                    { x: 17.48449, y: 106.6039649, busStop: true },
                    { x: 17.24835, y: 106.808579, busStop: true },
                    { x: 17.2174577, y: 106.7902981, busStop: true  },
                ]
            },
            {
                name: 'Tuyến B4: TP. Đồng Hới - Phong Nha',
                wayPoints: [
                    { x: 17.514807, y: 106.609429, busStop: true  },
                    { x: 17.4715095, y: 106.623992, busStop: true },
                    { x: 17.4744242, y: 106.6343608, busStop: true },
                    { x: 17.466898, y: 106.6044638, busStop: true },
                    { x: 17.4722561, y: 106.6042974, busStop: true },
                    { x: 17.4846466, y: 106.6039467, busStop: true },
                    { x: 17.5874065, y: 106.5336577, busStop: true },
                    { x: 17.6122503, y: 106.3039075, busStop: true  },
                ]
            }
        ]
    },
    mutations: {
        setMap(state, payload) {
            state.map = new window.google.maps.Map(payload.element, payload.options);
        },
    },
    actions: {

    }
})
