import Vue from 'vue'
import App from './App.vue'
import store from './store'

import iView from 'iview';
import 'iview/dist/styles/iview.css';
import vietnam from 'iview/dist/locale/vi-VN';

Vue.use(iView, { vietnam });

Vue.config.productionTip = false

new Vue({
      store,
  ...App,
}).$mount('#app')