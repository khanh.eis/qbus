module.exports = {
  productionSourceMap: false,
  devServer: {
    disableHostCheck: true,
    hot: true,
  headers: { "Access-Control-Allow-Origin": "*" }
  },
  configureWebpack: {
    node: {
      process: false,
    }
  }
}
